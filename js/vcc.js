﻿
$(document).ready(function () {
    // slider bài viết liên quan (news relate)
    $('.news-detail-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    // resize image big content news page 
    var widthBigContentNews = $("#news .big-content").width();
    var heightBigContentNews = widthBigContentNews * 0.3575;
    $("#news .big-content .big-img-news").css("height", heightBigContentNews + "px");

    // resize image view list content news page 
    //var widthListContentNews = $(".view-list .item").width();
    //var heightListContentNews = widthListContentNews * 0.4022;
    //console.log(widthListContentNews);
    //$(".view-list .item .item-image").css("height", heightListContentNews + "px");

    // resize image big content FengShui page
    var widthBigContentFengShui = $("#FengShui .big-content").width();
    var heightBigContentFengShui = widthBigContentFengShui * 0.3575;
    $("#FengShui .big-content .big-img-news").css("height", heightBigContentFengShui + "px");

    // resize image news relate
    var widthRelateContentNews = $(".news-detail-slider .item:nth-child(1) img").width();    
    var heightRelateContentNews = widthRelateContentNews * 0.4022;
    $(".news-detail-slider .item img ").css("height", heightRelateContentNews + "px");
});

$(window).resize(function () {
    // resize image big content news page 
    var widthBigContentNews = $("#news .big-content").width();
    var heightBigContentNews = widthBigContentNews * 0.3575;
    $("#news .big-content .big-img-news").css("height", heightBigContentNews + "px");

    // resize image big content FengShui page
    var widthBigContentFengShui = $("#FengShui .big-content").width();
    var heightBigContentFengShui = widthBigContentFengShui * 0.4307;
    $("#FengShui .big-content .big-img-news").css("height", heightBigContentFengShui + "px");

    // resize image news relate
    var widthRelateContentNews = $(".news-detail-slider .item:nth-child(1) img").width();
    var heightRelateContentNews = widthRelateContentNews * 0.4022;
    $(".news-detail-slider .item img ").css("height", heightRelateContentNews + "px");
});